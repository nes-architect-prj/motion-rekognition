import dotenv from "dotenv";
import { appendFileSync, readFileSync, writeFileSync } from "fs";
dotenv.config({path: `${__dirname}/../.env`});

export interface ColorInterface {
    label: string,
    color: string,
    force: string
}

export default async function colors(conf : ColorInterface){
    try{

        // Check if the color is already present in the env file
        if( Object.keys(process.env).includes(`COLOR_${conf.label}`) ){
            if(!Boolean(conf.force)){
                console.log("This label is already associated to a color in the .env file. Add option -f to overwrite");
            }
            else{
                let envData = readFileSync(`${__dirname}/../.env`).toString().split("\n");
                for(let i = 0; i < envData.length; i++){
                    let key = envData[i].split("=")[0];
                    if(key === `COLOR_${conf.label}`){
                        envData.splice(i, 1, `COLOR_${conf.label}=${conf.color}`);
                        writeFileSync(`${__dirname}/../.env`, envData.join("\n"));
                        break;
                    }
                }
            }
        }
        else{
            appendFileSync(`${__dirname}/../.env`, `COLOR_${conf.label}=${conf.color}\n`);
            console.log(`Color added ! ${conf.color} boxes will be drawed when ${conf.label} will be detected`);
        }


    }
    catch(e:any){
        console.error(e);
        process.exit(-1);
    }
}