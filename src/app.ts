#!/usr/bin/env node

import dotenv from "dotenv";
dotenv.config({path: `${__dirname}/../.env`});

import processImageRekognition, {ProcessParams} from "./process";
import {program} from "commander";
import configure, { ConfInterface } from "./configure";
import { existsSync } from "fs";
import colors, { ColorInterface } from "./colors";

if(!existsSync(`${__dirname}/../.env`)){
    throw new Error("Env file not found, please run 'configure' command to init it");
}

( async() => {

    // Define
    program
        .name("motion-rekognition")
        .description("Utility to analyse labels on triggered motions from MotionEYE with AWS Rekognition.")
        .version("0.0.1");

    // Commands
    program.command("process")
        .description("Process image passed in argument")
        .requiredOption("-u, --user <string>","The user of motioneye")
        .requiredOption("-s, --signature <string>","The user signature motioneye")
        .requiredOption("-i, --camera-id <string>","The camera ID of motioneye")
        .option("-h, --host <string>","The motioneye host url", <any>process.env.HOST)
        .requiredOption("-o, --output-image <string>","The destination image after completion")
        .action( async (options) => await processImageRekognition(<ProcessParams>options) );

    program.command("configure")
        .description("Configure the .env file of this program")
        .requiredOption("-a, --access-key-id <string>","The Access key of AWS account")
        .requiredOption("-s, --secret-key <string>","The Secret key of AWS account")
        .requiredOption("-r, --region <string>","The region used by the AWS Access & Secret keys")
        .option("-m, --max-rekognition <string>","The max rekognition api call by day to avoid huge billing on AWS (5000/month is under free tier)","160")
        .option("-h, --host <string>","The motioneye host url","http://localhost:8765")
        .option("-f, --filters <string>","Comma separated labels target list","")
        .option("-c, --min-confidence <string>","Define the minimum level of confidence to validate a rekognition","70")
        .option("--default-color <string>","Define the default color used to draw label's boxes on image","Orange")
        .action( async (options) => await configure(<ConfInterface>options));

    program.command("add-color")
        .description("Associate new color to a label")
        .requiredOption("-l, --label <string>","The targeted label, see the full list there https://docs.aws.amazon.com/rekognition/latest/dg/labels.html")
        .requiredOption("-c, --color <string>","The color to be associated, can be hex format or string format")
        .option("-f, --force","Update the color if already exists", false)
        .action( async (options) => await colors(<ColorInterface>options));


    program.parse();

    // await processImageRekognition(<any>process.argv[2]);

})()

