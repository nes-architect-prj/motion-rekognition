import dotenv from "dotenv";
import { writeFileSync } from "fs";
dotenv.config({path: `${__dirname}/../.env`});

export interface ConfInterface {
    accessKeyId: string,
    secretKey: string,
    region: string,
    minConfidence: string,
    defaultColor: string,
    filters: string,
    host: string,
    maxRekognition : string
}

export default async function configure(conf : ConfInterface){
    try{

        let envData = `ACCESS_KEY=${conf.accessKeyId}\n`
                    + `SECRET_KEY=${conf.secretKey}\n`
                    + `DEFAULT_REGION=${conf.region}\n`
                    + `HOST=${conf.host}\n`
                    + `MIN_CONFIDENCE=${conf.minConfidence}\n`
                    + `COLOR_DEFAULT=${conf.defaultColor}\n`
                    + `FILTERS=${conf.filters}\n`;
                    + `MAX_REKOGNITION=${conf.maxRekognition}\n`;

        writeFileSync(`${__dirname}/../.env`, envData);

    }
    catch(e:any){
        console.error(e);
        process.exit(-1);
    }
}