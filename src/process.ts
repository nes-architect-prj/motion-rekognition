import dotenv from "dotenv";
dotenv.config({path: `${__dirname}/../.env`});

import {Rekognition} from "@aws-sdk/client-rekognition";
import { existsSync, mkdirSync, readFileSync, writeFileSync } from "fs";
import gm, { ImageInfo } from "gm";
import path from "path";
import axios from "axios";

// Color schema to 
let colorSchema : {[key:string]:string} = {
    UNKNOWN: "Orange"
};

// Filters retrived from the .env file 
let filters : [string] = (process.env.FILTERS) ? <any>process.env.FILTERS.split(",") : [];

export interface ProcessParams {
    host: string,
    user: string,
    signature: string,
    cameraId: string,
    outputImage: string
}

export default async function processImageRekognition(params : ProcessParams) {

    let client = new Rekognition({
        credentials: {
            accessKeyId: <any>process.env.ACCESS_KEY,
            secretAccessKey: <any>process.env.SECRET_KEY,
        },
        region: <any>process.env.DEFAULT_REGION
    });

    try{

        // Downloading the latest snapshot and save it buffer a temp file (based on timestamp)
        let snapshotUrl = `${<any>process.env.HOST}/picture/${params.cameraId}/current/?_username=${params.user}&_signature=${params.signature}`;
        let snapBuffer = (await axios.get(snapshotUrl, {responseType: "arraybuffer"})).data;

        let result = await client.detectLabels({
            MinConfidence: Number.parseInt(<any>process.env.MIN_CONFIDENCE),
            Image: {
                Bytes: snapBuffer
            }
        })

        // writeFileSync("./temp.json", JSON.stringify(result.Labels,null,"\t") );

        if(result.Labels){

            console.log(`Labels founds without filters : ${result.Labels?.length}`);

            // Filter labels if filters founds
            let labels = (filters.length > 0) ? result.Labels.filter( (l:any) => filters.includes(l.Name) ) : result.Labels;
            console.log(`Labels founds with filters : ${labels.length}`);
            if(labels.length === 0){
                console.log("No labels has been identified, maybe decresae the MIN_CONFIDENCE value ?")
                return process.exit(0);
            }

            // Get the image to be process from the buffered image
            let image = gm(snapBuffer);
            let imgInfo : ImageInfo = await ( new Promise((resolve,reject) => {
                image.identify( (err,info) => {
                    if(err) reject(false)
                    else resolve(info);
                })
            }) )

            // Check if the path exists, else create it
            if( !existsSync(path.dirname(params.outputImage)) ){
                console.log(`${path.dirname(params.outputImage)} not existing, creating it`);
                mkdirSync(path.dirname(params.outputImage), {recursive: true});
            }

            // Browse all Labels
            for(let i = 0; i < labels.length; i++){

                // Current label
                let label = labels[i];
                let label_confidence = label.Confidence;

                // Check if info exists
                if(!label.Name) continue;
                if(!label.Instances) continue;
                if(label.Instances.length === 0){
                    console.log(`[x] Label ${label.Name} ignored : no instances founds`);
                    continue;
                }

                // Get instance Name
                let label_name =  label.Name;

                // Define the color from FILTERS
                let colorStroke = (Object.keys(process.env).filter( (k:string) => k.toString().startsWith("COLOR_") ).includes("COLOR_"+label_name)) ? <string>process.env["COLOR_"+label_name] : <string>process.env["COLOR_DEFAULT"];

                // Output
                console.log(`Processing label : ${label_name} [colour ${colorStroke}] identified at ${Math.round(<number>label_confidence)}%`);
                
                // Browse instances label now
                for(let j = 0; j < label.Instances.length; j++){


                    // Get the instance "i" from label.Instances
                    let instance = label.Instances[j]; 

                    // Retrieve width, height and positions of the box to be drawing
                    let box = {
                        w: Math.round(<any>instance.BoundingBox?.Width*imgInfo.size.width),     
                        h: Math.round(<any>instance.BoundingBox?.Height*imgInfo.size.height),
                        left: Math.round(<any>instance.BoundingBox?.Left*imgInfo.size.width),
                        top: Math.round(<any>instance.BoundingBox?.Top*imgInfo.size.height)
                    };

                    // Draw on the image the box with apropriate colors
                    image
                        .stroke( colorStroke )
                        .fill("None")
                        .drawRectangle(box.left, box.top, box.left+box.w, box.top+box.h)
                        .fontSize(14)
                        .stroke( colorStroke )
                        .fill( colorStroke )
                        .drawText(box.left, box.top+box.h+14, `${label_name} (${Math.round(<number>label_confidence)}%)`)
                        .quality(100)

                }

            }

            // Pass by a promise to write the modifications
            let promise = new Promise( (resolve,reject) => {
                    
                image
                    .write(params.outputImage, (err) => {
                        if(err){
                            console.log(err);
                            reject(false);
                        }
                        else resolve(true);
                    });
            })
            await promise;

        }

    }
    catch(e:any){
        console.error(e);
        process.exit(-1);
    }

}